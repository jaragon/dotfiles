# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable bi-weekly auto-update checks
DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

 #Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git git-extras extract vi-mode gnu-utils colorize ssh-agent tmux urltools)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...

ZSH_TMUX_AUTOSTART=true

# Self-contained theme
PROMPT='$FG[245]%c %{$reset_color%}'
RPROMPT='$(git_prompt_info)'

ZSH_THEME_GIT_PROMPT_PREFIX="<"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}*%{$reset_color%}>"
ZSH_THEME_GIT_PROMPT_CLEAN=">"

export EDITOR=vim
export PAGER=less
export LANG="en_US.UTF-8"
export HOSTNAME="dodi"

alias g='git'
alias gs='git status'
alias ga='git add .'
alias gc='git commit -m'
alias gp='git push'

alias zr='source ~/.zshrc'

# Don't pause terminal on <c-s>
stty -ixon

# Set input on vi-mode
set -o vi

# <c-l> clear screen
bindkey '\C-l' clear-screen

# <c-p> check for partial match in history
bindkey '\C-p' dynamic-complete-history

# <c-n> cycle through list of partial matches
bindkey '\C-n' menu-complete

# keeps history in sync across terminals
export PROMPT_COMMAND="history -a; history -n"


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
