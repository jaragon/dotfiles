" vimrc of jerico, stolen from various sources

set nocompatible

set directory=$HOME/.vim/swap,$HOME/vimfiles/swap,/tmp
set backupdir=$HOME/.vim/backup,$HOME/vimfiles/backup,/tmp

set runtimepath+=$HOME/.vim/bundle/vundle/
set runtimepath+=$HOME/vimfiles/vundle/

call plug#begin('~/.vim/bundle')

Plug 'kien/ctrlp.vim'
Plug 'vim-scripts/xoria256.vim' 
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

"Plug 'christoomey/vim-tmux-navigator'
"Plug 'lastpos.vim' 
"Plug 'othree/html5.vim', { 'for': ['html', 'php'] }
"Plug 'digitaltoad/vim-jade', { 'for': 'jade' }
"Plug 'tpope/vim-haml', { 'for', 'haml' }
"Plug 'hallison/vim-markdown', { 'for', 'markdown' }
"Plug 'terryma/vim-multiple-cursors'
"Plug 'davidoc/taskpaper.vim'
"Plug 'mattn/emmet-vim'
"Plug 'tpope/vim-fugitive'
"Plug 'groenewege/vim-less'
"Plug 'scrooloose/nerdcommenter'
"Plug 'vim-stylus'
"Plug 'ZoomWin' 
"Plug 'godlygeek/tabular'
"Plug 'tpope/vim-surround'
"Plug 'ack.vim'
"Plug 'StanAngeloff/php.vim'
"Plug 'mattn/zencoding-vim'
"Plug 'wincent/Command-T'
"Plug 'vimoutliner/vimoutliner'
"Plug 'tlib'
"Plug 'tomtom/viki_vim'

call plug#end()

syntax on
colorscheme xoria256
filetype plugin indent on

" Tabbing
set tabstop=2     " Specify width of a tab character
set softtabstop=2 " Number of spaces to use when performing editing operations
set shiftwidth=2  " Determines the amount of space to add or remove using indentation
set expandtab     " Use spaces instead of real <Tab>
set autoindent   " Copy indent from the current line when starting a new line
set breakindent

" Formatting
set wrap
set linebreak
set nolist
"set colorcolumn=80
"set nuw=6
"set columns=86
"set textwidth=80
"set formatoptions+=tcq

" Searching
set wrapscan      " Wrap searches
set ignorecase    " Ignore search term case...
set smartcase     " ... unless term contains an uppercase character
set incsearch     " Highlight search...
set showmatch     " ... results
set hlsearch      " ... as you type

set history=100
set backspace=indent,eol,start
set encoding=utf-8
set wildmenu      " Command-line completion operates in an enhanced mode.
set hidden        " Only hides buffer when hidden (not close)
set autoread      " Read file again when changed outside vim
set autochdir     " Change the current working directory to the same file opened
set backup

set wildignore+=*/.sass-cache/*
set wildignore+=*/bower_components/*

" GUI
set guifont=DejaVu\ Sans\ Mono\ 12,Consolas:h10
set guioptions=Cs
set laststatus=2
set ruler

map <Leader>n :NERDTreeToggle<CR>

imap <C-k> <Up>
imap <C-j> <Down>
imap <C-h> <Left>
imap <C-l> <Right>

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" emmet
let g:user_emmet_settings = {
\    'indentation' : '  '
\}

let g:use_emmet_complete_tag = 1
let g:user_emmet_mode='a'  "enable all function in all mode.

" ctrlp 

let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](git|hg|svn|node_modules|bower_components|build)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ }
